<?
ini_set('display_errors', 'On');
 if($_SERVER["REQUEST_METHOD"] == "POST")
{
 // Функция отправки email
 function send_mail($from,$to,$subject,$body)
{
  $charset = 'utf-8';
  mb_language("ru");
  $headers  = "MIME-Version: 1.0 \r\n" ;
  // $headers .= "From: <".$from."> \r\n";
  $headers .= "Reply-To: <".$from."> \r\n";
  $headers .= "Content-Type: text/html; charset=$charset \r\n";

  $subject = '=?'.$charset.'?B?'.base64_encode($subject).'?=';

  mail($to,$subject,$body,$headers);
}

function send_email_sendgrid($from,$to,$subject,$body)
{
  $url = 'https://api.sendgrid.com/';
  $user = 'rubikal-website';
  $pass = '1password';

$params = array(
    'api_user'  => $user,
    'api_key'   => $pass,
    'to'        => $to,
    'subject'   => 'info-Rubikal',
    'html'      => $body,
    'text'      => $body,
    'from'      => $from,
  );


$request =  $url.'api/mail.send.json';

// Generate curl request
$session = curl_init($request);
// Tell curl to use HTTP POST
curl_setopt ($session, CURLOPT_POST, true);
// Tell curl that this is the body of the POST
curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
// Tell curl not to return headers, but do return the response
curl_setopt($session, CURLOPT_HEADER, false);
// Tell PHP not to use SSLv3 (instead opting for TLS)
curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

// obtain response
$response = curl_exec($session);
curl_close($session);

}

session_start();

$email = $_POST["feedback_email"];
$check = $_POST["feedback_subscribe"];
$txt = $_POST["feedback_txt"];

$error = array();

if (!preg_match("/^(?:[a-z0-9]+(?:[-_.]?[a-z0-9]+)?@[a-z0-9_.-]+(?:\.?[a-z0-9]+)?\.[a-z]{2,5})$/i",trim($email))){ $error[] = "Use correct email!"; }
if (strlen($txt) == 0){ $error[] = "No questions? :)"; }



if (count($error))
{
    echo implode('<br />',$error);
}else
{
    unset($_SESSION['result_key']);
    // Отправка email
    send_email_sendgrid($email,
               'careers@rubikal.com',
               $subject,
               $txt);

    echo 'true';
}

}
?>
