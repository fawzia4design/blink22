$(function() {

});

$(document).ready(function() {
	var owl = $('.single-carousel');
	owl.owlCarousel({
		nav: true,
		loop: true,
		items: 1,
	});

    var autoOwl = $('.testimonial-carousel');
    autoOwl.owlCarousel({
        nav: true,
        loop: true,
        items: 1,
        autoplay: true,        
    });

    var clientOwl = $('.client-carousel');
    clientOwl.owlCarousel({
        nav: true,
        loop: true,
        items: 1,
        autoplay: true,
        dotsContainer: '#carousel-custom-dots',
    });
    
    $('.owl-dot').click(function () {
        clientOwl.trigger('to.owl.carousel', [$(this).index(), 300]);
    });

	$('.back-to-top').click(function(){ 
        $("html, body").animate({ scrollTop: 0 }, 600); 
        return false; 
    }); 

    $(".positions-btn").click(function() {
	    $('html, body').animate({
	        scrollTop: $(".positions-section").offset().top
	    }, 600);
	});
});

$(".mobile-menu").click(function(){
    $(".main-nav ul").toggleClass("show");
});

/*$(document).ready(function(){ 
    $(window).scroll(function(){ 
        if ($(this).scrollTop() > 100) { 
            $('#scroll').fadeIn(); 
        } else { 
            $('#scroll').fadeOut(); 
        } 
    }); 
});*/

/* Show and hide header */
// Hide Header on on scroll down
var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = $('#header').outerHeight();

$(window).scroll(function(event){
    didScroll = true;
});

setInterval(function() {
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
}, 250);

function hasScrolled() {
    var st = $(this).scrollTop();
    
    // Make sure they scroll more than delta
    if(Math.abs(lastScrollTop - st) <= delta)
        return;
    
    // If they scrolled down and are past the navbar, add class .nav-up.
    // This is necessary so you never see what is "behind" the navbar.
    if (st > lastScrollTop && st > navbarHeight){
        // Scroll Down
        $('#header').removeClass('header-down').addClass('header-up');
    } else {
        // Scroll Up
        if(st + $(window).height() < $(document).height()) {
            $('#header').removeClass('header-up').addClass('header-down');
        }
    }
    
    lastScrollTop = st;
}